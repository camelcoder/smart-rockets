# Smart Rockets
A OpenGL implementation of the Smart Rockets genetic algorithm based on [The Coding Train's](https://www.youtube.com/user/shiffman) video: ["Coding Challenge #29: Smart Rockets in p5.js"](https://youtu.be/bGz7mv2vD6g).

## 🖼️Pics
![](pic.png)
